package priv.jh.web;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns="/hello")
public class HelloServlet implements Servlet{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub 销毁时候的方法
		System.out.println("销毁时候的方法");
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub  获取配置对象
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub 获取信息
		return null;
	}

	@Override
	public void init(ServletConfig arg0) throws ServletException {
		// TODO Auto-generated method stub  初始化servlet环境
		System.out.println("初始化环境");
	}

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
		// TODO Auto-generated method stub  提供服务
		System.out.println("servlet提供服务");
		
	}

	public HelloServlet() {
		System.out.println("aa");
	}
	

}
